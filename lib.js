'use strict';

const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB.DocumentClient({'convertEmptyValues': true});

module.exports.sendSMS = sendSMS;
function sendSMS(snsPros) {
    return new Promise((resolve, reject) => {

        const sns = new AWS.SNS({'region': process.env.SES_REGION});

        sns.subscribe({
            Protocol: 'sms',
            TopicArn: process.env.SNS_TOPIC_ARN,
            Endpoint: snsPros.contact
        }, function(error, data) {
            if (error) {
                return reject(error);
            }


            const SubscriptionArn = data.SubscriptionArn;

            const params = {
                TargetArn: process.env.SNS_TOPIC_ARN,
                Message: snsPros.text,
                Subject: snsPros.subject,
            };

            sns.publish(params, function(err_publish, data) {
                if (err_publish) {
                    console.log('Error sending a message', err_publish);

                } else {
                    console.log('Sent message:', data.MessageId);
                }

                const params = {
                    SubscriptionArn: SubscriptionArn
                };

                sns.unsubscribe(params, function(err, data) {
                    if (err) {
                        console.log("err when unsubscribe", err);
                    }
                    return resolve('success');
                });
            });
        });
     });
}

module.exports.sendEmail = sendEmail;
function sendEmail(emailProps) {
    return new Promise((resolve, reject) => {
        const ses = new AWS.SES({'region': process.env.SES_REGION});

        const requestParameters = {
            Destination: {
                ToAddresses: [emailProps.contact]
            },
            Message: {
                Body: {
                    Html: {
                        Data: emailProps.text
                    }
                },
                Subject: {
                    Data: emailProps.subject
                }
            },
            Source: process.env.FROM_EMAIL,
            ReturnPath: process.env.FROM_EMAIL
        };

        ses.sendEmail(requestParameters, (err, result) => {
            if (err) {
                return reject(err);
            }

            return resolve('success');
        })
    })
}

module.exports.saveMessage = (message) => {
    return new Promise((resolve, reject) => {
        if (!message) {
            return reject('Invalid message');
        }

        const uuid = require('node-uuid');

        if (!message['_id']) {
            message['_id'] = uuid.v1();
        }

        const now = new Date().toISOString();

        if (!message['createdAt']) {
            message['createdAt'] = now;
        }
        message['updatedAt'] = now;

        dynamodb.put(
            {
                'TableName': process.env.STAGE + '_messages',
                'Item': message
            },
            (err, data) => {
                if (err) return reject(err);

                return resolve('success');
            }
        );
    })
};

module.exports.getMessages = (contact) => {
    return new Promise((resolve, reject) => {
        const dynamodb = new AWS.DynamoDB.DocumentClient();

        const dynamoParams = {
            'TableName': process.env.STAGE + '_messages',
            'KeyConditionExpression': 'contact = :contact',
            'ExpressionAttributeValues': {
                ':contact': contact
            }
        };

        let result = [];
        const dynamoExecute = () => {
            dynamodb.query(dynamoParams, (err, data) => {
                if (err) {
                    return reject(err);
                } else {
                    result = result.concat(data.Items);

                    if (data.LastEvaluatedKey) {
                        dynamoParams.ExclusiveStartKey = data.LastEvaluatedKey;
                        dynamoExecute();
                    } else {
                        return resolve(result);
                    }
                }
            });
        };

        dynamoExecute();
    })
};

