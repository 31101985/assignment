'use strict';

const headers = {}
headers['Access-Control-Allow-Origin'] = '*';

const successResponse = {
    'statusCode': '200',
    'headers': headers,
    'body': null
}
const errorResponse = {
    'statusCode': '500',
    'headers': headers,
    'body': null
}

const lib = require('./lib');

module.exports.handler = (event, context, callback) => {
    console.log('Event::: ' + JSON.stringify(event));

    if (event) {
        if (event['requestContext'] && event['requestContext']['resourcePath'] && event['requestContext']['httpMethod']) {
            switch (event['requestContext']['httpMethod']) {
                case 'POST':
                    return publishMessage(event, context, callback);
                case 'GET':
                    return retrieveMessages(event, context, callback);
                default:
                    break;
            }
        }
    }

    errorResponse.body = JSON.stringify({
        'error': 'Invalid request!'
    });
    return callback(null, errorResponse);
}

function publishMessage(event, context, callback) {

    const message = JSON.parse(event['body']);

    if (!message) {
        errorResponse.body = JSON.stringify({
            'error': 'Message details are missing!'
        });
        return callback(null, errorResponse);
    }
    if (!message.type || (message.type.toUpperCase() !== 'SMS' && message.type.toUpperCase() !== 'EMAIL')) {
        errorResponse.body = JSON.stringify({
            'error': 'Invalid message type!'
        });
        return callback(null, errorResponse);
    }
    if (!message.text) {
        errorResponse.body = JSON.stringify({
            'error': 'Invalid message text!'
        });
        return callback(null, errorResponse);
    }
    if (!message.contact) {
        errorResponse.body = JSON.stringify({
            'error': 'Invalid contact!'
        });
        return callback(null, errorResponse);
    }
    if (!message.subject) {
        errorResponse.body = JSON.stringify({
            'error': 'Invalid subject!'
        });
        return callback(null, errorResponse);
    }
    if (message.type.toUpperCase() === 'SMS') {
        return lib.sendSMS(message).then((resp) => {
            message.contact = message.contact.substring(1);
            return lib.saveMessage(message).then((response) => {
                successResponse.body = JSON.stringify({
                    'response': 'SMS sent and text stored successfully!'
                });
                return callback(null, successResponse);
            }).catch((err) => {
                errorResponse.body = JSON.stringify(err);
                return callback(null, errorResponse);
            }).catch((err) => {
                errorResponse.body = JSON.stringify(err);
                return callback(null, errorResponse);
            })
        })
    } else {
        return lib.sendEmail(message).then((resp) => {
            return lib.saveMessage(message).then((response) => {
                successResponse.body = JSON.stringify({
                    'response': 'EMAIL sent and text stored successfully!'
                });
                return callback(null, successResponse);
            }).catch((err) => {
                errorResponse.body = JSON.stringify(err);
                return callback(null, errorResponse);
            }).catch((err) => {
                errorResponse.body = JSON.stringify(err);
                return callback(null, errorResponse);
            })
        })
    }
}

function retrieveMessages(event, context, callback) {
    const message = event['queryStringParameters']

    if (!message) {
        errorResponse.body = JSON.stringify({
            'error': 'Invalid input!'
        });
        return callback(null, errorResponse);
    }
    if (!message.contact) {
        errorResponse.body = JSON.stringify({
            'error': 'Invalid contact!'
        });
        return callback(null, errorResponse);
    }

    return lib.getMessages(message.contact).then((result) => {
        successResponse.body = JSON.stringify(result);
        return callback(null, successResponse);
    }).catch((err) => {
        errorResponse.body = JSON.stringify(err);
        return callback(null, errorResponse);
    })
}